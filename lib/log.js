const fs = require('fs');
const env = process.env.NODE_ENV || 'development';
const bunyan = require('bunyan')
const path = require('path')
const mkdirp = require('mkdirp')
//
// If we're not in production then log to the `console` with the format:
// `${info.level}: ${info.message} JSON.stringify({ ...rest }) `
// 
if (env !== 'production') {
    // logger.add(new transports.Console());
}
function stringify(msg, object) {
    return object ? (msg + JSON.stringify(object)) : msg;
}
let self;
class LoggerService {
    constructor() {
        this.baseDir = ''
        this.logDir = ''
        this.logger = null
        self = this;
    }
    createLogger(name, level, suffix, baseDir, logDir) {
        name = (name && name !== "") ? name : "YNTNodeUtils";
        level = (level && level !== "") ? level : (env === 'development' ? 'debug' : 'info');
        self.logDir = logDir;
        self.baseDir = baseDir;
        self.logger = bunyan.createLogger({
            name: name,
            streams: [{
                level: level,
                stream: process.stdout            // log INFO and above to stdout
            }, {
                level: level,
                path: createActiveLog(baseDir, logDir, suffix)
            }]
        });
    }
    startLogCleanUpTask() {
        const logUtils = require('./log_utils')
        logUtils.startLogCleanUpTask(path.join(self.getBaseDir(), self.getLogDir()));
    }
    info(msg, object) {
        var formatted = stringify(msg, object);
        self.logger.info(formatted);
    }
    error(msg, object) {
        var formatted = stringify(msg, object);
        self.logger.error(formatted);
    }
    warn(msg, object) {
        var formatted = stringify(msg, object);
        self.logger.warn(formatted);
    }
    debug(msg, object) {
        var formatted = stringify(msg, object);
        self.logger.debug(formatted);
    }
    trace(msg, object) {
        var formatted = stringify(msg, object);
        self.logger.trace(formatted);
    }
    fatal(msg, object) {
        var formatted = stringify(msg, object);
        self.logger.fatal(formatted);
    }
    getBaseDir() {
        return self.baseDir;
    }
    getLogDir() {
        return self.logDir;
    }
}

function createActiveLog(baseDir, logDirName) {
    const logDirPath = path.join(baseDir, logDirName);
    if (!fs.existsSync(logDirPath)) mkdirp.sync(logDirPath);
    rename(logDirPath);
    var newLogFilePath = path.join(logDirPath, newLogFilename());
    return newLogFilePath;
}

function rename(logDir) {
    var files = fs.readdirSync(logDir);
    files.forEach(function (file) {
        if (file.endsWith(".active"))
            fs.renameSync(path.join(logDir, file),
                path.join(logDir, file.split(".active")[0]));
    });
}

function newLogFilename() {
    return new Date().toISOString()
        .replace(/:/g, "_")
        .concat("_AGENT")
        .concat(".log.active");
}

module.exports = new LoggerService();