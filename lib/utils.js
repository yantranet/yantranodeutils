const log = require('./log.js');
const fs = require('fs-extra');
const path = require('path');
const { v4: uuidv4 } = require('uuid');
const { v5: uuidv5 } = require('uuid');
const encryptor = require('file-encryptor');

let utils = {
    createDir: function (path) {
        fs.emptyDir(path, function (err) {
            if (err) log.error(err)
            else log.info('successfully created ' + path)
        });
    },

    createDirSync: function (path) {
        fs.emptyDirSync(path);
    },

    getFilesizeInBytes: function (filePath) {
        if (!fs.existsSync(filePath)) return 0;
        const stats = fs.statSync(filePath)
        const fileSizeInBytes = stats.size
        return fileSizeInBytes
    },

    getDirSizeSync: function (dirPath) {
        return getDirSizeAsync(dirPath).then(size);
    },

    getDirSizeAsync: function (dirPath) {
        return new Promise(function (resolve, reject) {
            utils.getFilesInDirAsync(dirPath)
                .then(function (files) {
                    var promises = [];
                    files.forEach((file) => promises.push(utils.getFileSizeInBytesAsync(path.join(dirPath, file))));
                    return Promise.all(promises);
                })
                .then(sizes => resolve(sizes.reduce(function (a, b) { return a + b; }, 0)))
                .catch(error => reject(error));
        });
    },

    getFilesInDirAsync: function (dir) {
        return new Promise(function (resolve, reject) {
            fs.readdir(dir, function (err, files) {
                if (err) reject(err);
                else resolve(files);
            })
        });
    },

    getFileSizeInBytesAsync: function (file) {
        return new Promise(function (resolve, reject) {
            utils.getFileStatsAsync(file)
                .then(function (stats) {
                    if (stats.isDirectory()) utils.getFileSizeInBytesAsync(file);
                    else if (stats.isFile()) resolve(stats.size);
                }).catch(error => reject(error));
        });
    },

    getFileStatsAsync: function (file) {
        return new Promise(function (resolve, reject) {
            fs.stat(file, function (err, stats) {
                if (err) reject(err);
                else resolve(stats);
            })
        });
    },

    generateRandomUUID: function () { return uuidv4(); },

    generateUUID: function (name, namespace) { return uuidv5(name, namespace); },

    encryptFile: function (key, name, path) {
        // Encrypt file.
        encryptor.encryptFile(path, name + '.dat', key, function () {
            log.info("Utils ::: [ " + name + " ] file is encrypted successfully ... !!");
        });
    }
}
module.exports = utils;