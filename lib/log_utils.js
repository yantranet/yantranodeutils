const fs = require('fs');
const { readdirSortedByCreationTime, getFilesizeInBytes } = require('./utils.js');
const LOG_DIR_SIZE_THRESHOLD = 10 * 1024 * 1024;
const { bindCallback, of } = require('rxjs');
const { mergeMap, map, filter } = require('rxjs/operators');
const cron = require('cron').CronJob;
const log = require('./log.js');

let self;
class LogUtils {
    constructor() {
        self = this
    }
    cleanUp(logDirPath) {
        const fsUnlinkObs = bindCallback(fs.unlink, (error) => error);
        const delFilesObs = (filePath) => of(filePath)
            .pipe(
                filter(filePath => !filePath.endsWith('.active')),
                mergeMap(filePath => fsUnlinkObs(filePath))
            );
        const logThresholdReachedObs = of(logDirPath)
            .pipe(
                map(logDirPath => getFilesizeInBytes(logDirPath)),
                filter(size => size >= LOG_DIR_SIZE_THRESHOLD)
            );
        return logThresholdReachedObs
            .pipe(
                mergeMap(() => readdirSortedByCreationTime(logDirPath)),
                mergeMap(filePath => delFilesObs(filePath))
            );
    }
    startLogCleanUpTask(logDirPath, intervalAsCron = "*/10 * * * *") {
        return new cron(intervalAsCron, function () {
            log.info("LogCleanUpTask Started. Checking if log dir size > " + LOG_DIR_SIZE_THRESHOLD / (1024 * 1024) + " MB")
            self.cleanUp(logDirPath).subscribe();
        }, null, true, null, null, false);
    }
}

module.exports = new LogUtils();